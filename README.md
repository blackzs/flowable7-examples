# Flowable7 Spring Boot Demo

Flowable7+Springboot3+Flowable6的流程设计器(不需要idm)。

启动后的入口地址
http://domain:port/designer/index.html#/editor/

保存后修改流程的地址
http://domain:port/designer/index.html#/editor/{modelId}

启动一个流程
http://domain:port/flow/start/{modelId}

完成一个任务
http://domain:port/flow/complate/{taskId}

详细说明可关注公众号

![image](公众号.jpg)